package web.engine;

import web.config.ConfigProvider;

import java.util.Arrays;

public enum DriverType {
    chrome, ff, edge, safari;

    public static DriverType getFromConfig() {
        String driverType = ConfigProvider.CONFIG_PROPS.driverType();
        return Arrays.stream(DriverType.values())
                .filter(it -> it.name().equals(driverType))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Provided driver type is not supported\n" +
                        "Driver Type: " + driverType));
    }
}
