package web.engine;

import com.codeborne.selenide.Configuration;
import web.config.ConfigProvider;
import web.engine.drivers.ChromeDriverProvider;
import web.engine.drivers.EdgeDriverProvider;
import web.engine.drivers.FirefoxDriverProvider;
import web.engine.drivers.SafariDriverProvider;

import java.time.Duration;

public class DriverConfigurator {
    public static void configure() {
        Configuration.timeout = Duration.ofSeconds(ConfigProvider.CONFIG_PROPS.driverTimeout()).toMillis();
        Configuration.startMaximized = true;
        Configuration.reopenBrowserOnFail = true;
        Configuration.savePageSource = false;
        Configuration.screenshots = false;
        Configuration.browser = getDriverClassName();
    }

    private static String getDriverClassName() {
        DriverType driverType = DriverType.getFromConfig();
        switch (driverType) {
            case chrome:
                return ChromeDriverProvider.class.getName();
            case ff:
                return FirefoxDriverProvider.class.getName();
            case edge:
                return EdgeDriverProvider.class.getName();
            case safari:
                return SafariDriverProvider.class.getName();
            default:
                throw new IllegalArgumentException(String.format("No implementation for provided driver type: " +
                        "Driver Type[%s]", driverType));
        }
    }
}
