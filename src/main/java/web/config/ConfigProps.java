package web.config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "file:${env.config.path}",
        "file:${common.config.path}",
        "file:${testrail.config.path}"
})

public interface ConfigProps extends Config {
    @Key("web.url")
    String webUrl();

    @Key("driver.timeout")
    long driverTimeout();

    @Key("driver.type")
    String driverType();
}