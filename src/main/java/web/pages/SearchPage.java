package web.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class SearchPage {

    public SearchPage isProductExist(String productName){
        $x(String.format("//section//*[contains(text(),'%s')]",productName)).shouldBe(Condition.exist);
        return this;
    }
}
