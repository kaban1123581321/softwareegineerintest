package web.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import web.config.ConfigProvider;

import static com.codeborne.selenide.Selenide.*;

public class HomePage {
    private String baseUrl;

    SelenideElement header = $x("//img[@alt='Rozetka Logo']"),
                    searchField = $(".header-search .search-form__input"),
                    submitSearchBtn = $(".header-search .search-form__submit");

    public HomePage() {
        baseUrl = ConfigProvider.CONFIG_PROPS.webUrl();
        open(baseUrl);
    }

    public HomePage isPageOpened(){
        header.shouldBe(Condition.visible);
        return this;
    }

    public SearchPage searchProduct(String productName){
        searchField.setValue(productName);
        submitSearchBtn.click();
        return new SearchPage();
    }

}
