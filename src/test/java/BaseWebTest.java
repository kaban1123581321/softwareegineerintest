import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import web.engine.DriverConfigurator;
import web.engine.DriverUtils;

public class BaseWebTest {
    @BeforeMethod(alwaysRun = true, description = "Initialize web driver configuration")
    protected void beforeMethod() {
        DriverConfigurator.configure();//this method doesn't open the browser yet
    }

    @AfterMethod(alwaysRun = true, description = "Close driver")
    protected void tearDown() {
        DriverUtils.stop();
    }
}
