import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;
import web.pages.HomePage;

import static web.data.Constants.FIRST_TESTNG_GROUP;
import static web.data.Constants.SECOND_TESTNG_GROUP;

public class Tests extends BaseWebTest{
    HomePage homePage;
    private static final String productName = "Samsung";

    @TmsLink("1")
    @Test(groups = FIRST_TESTNG_GROUP)
    public void loginTest(){
        homePage = new HomePage();
        homePage.isPageOpened();
    }

    @TmsLink("2")
    @Test(groups = SECOND_TESTNG_GROUP)
    public void searchTest(){
        homePage = new HomePage();
        homePage.searchProduct(productName)
                .isProductExist(productName);
    }
}
