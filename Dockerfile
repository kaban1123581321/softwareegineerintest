FROM centos:7
MAINTAINER Borys Menko <bmenk@softserveinc.com>

ENV JAVA_VERSION="1.8.0" \
    GRADLE_VERSION="5.2.1" \
    JAVA_HOME="/usr/lib/jvm/java" \
    GRADLE_HOME="/opt/gradle" \
    PATH="/opt/gradle/bin/:$PATH" \
    TZ="America/New_York"

RUN yum install -y epel-release
RUN yum install -y fluxbox
RUN yum install -y git
RUN yum install -y gnupg2
RUN yum install -y java-${JAVA_VERSION}-openjdk
RUN yum install -y java-${JAVA_VERSION}-openjdk-devel
RUN yum install -y unzip
RUN yum install -y wget
RUN yum install -y x11vnc
RUN yum install -y xorg-x11-server-Xvfb
RUN echo "Downloading Gradle..."
RUN wget -q https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
RUN unzip -q gradle-${GRADLE_VERSION}-bin.zip
RUN mv gradle-${GRADLE_VERSION} ${GRADLE_HOME}
RUN rm -f gradle-${GRADLE_VERSION}-bin.zip
RUN mkdir -p ${HOME}/.gradle
RUN chown -R 1001:0 ${HOME}
RUN chmod -R g+rw ${HOME}
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
RUN yum localinstall -y google-chrome-stable_current_x86_64.rpm
RUN rm -f google-chrome-stable_current_x86_64.rpm
RUN yum clean all -y
